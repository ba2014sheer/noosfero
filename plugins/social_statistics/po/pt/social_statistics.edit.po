# translation of noosfero.po to
# Krishnamurti Lelis Lima Vieira Nunes <krishna@colivre.coop.br>, 2007.
# noosfero - Brazilian Portuguese translation
# Copyright (C) 2007,
# Forum Brasileiro de Economia Solidaria <http://www.fbes.org.br/>
# Copyright (C) 2007,
# Ynternet.org Foundation <http://www.ynternet.org/>
# This file is distributed under the same license as noosfero itself.
# Joenio Costa <joenio@colivre.coop.br>, 2008.
#
#
msgid ""
msgstr ""
"Project-Id-Version: 1.3~rc2-1-ga15645d\n"
"PO-Revision-Date: 2014-12-18 18:40-0200\n"
"Last-Translator: Luciano Prestes Cavalcanti <lucianopcbr@gmail.com>\n"
"Language-Team: Portuguese <https://hosted.weblate.org/projects/noosfero/"
"noosfero/pt/>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 2.0\n"

#: ../lib/social_statistics_plugin.rb:4
#: ../views/layouts/blazer/application.html.erb:6
msgid "Social Statistics"
msgstr "Estatísticas Sociais"

#: ../lib/social_statistics_plugin.rb:8
msgid "Provides customized social statistics graphs and checks."
msgstr "Provê checagens e gráficos sociais estatísticos customizados."

#: ../lib/social_statistics_plugin.rb:18
msgid "Stats"
msgstr "Estatísticas"

#: ../lib/social_statistics_plugin.rb:22
msgid "Manage the environment statistics."
msgstr "Gerenciar as estatísticas do ambiente."
